from sentrycommon import PipelineConfig
from sentrycommon import SentryAWSClients
from sentrycommon import Logger
import logging
import time

LogSetup = Logger.Logger_Setup()
logger = logging.getLogger(__name__)
LogSetup.set_level(logger)


def get_timestamps_past_n_days(num_days):
    now = time.time()
    timestamps = []
    for day in range(num_days, 0, -1):
        timestamp = now - day * 86400
        timestamps.append(timestamp)

    return timestamps


def get_m_keys_per_day_for_last_n_days(sentry_id, days_back, trim_to=5):
    logger.debug('Getting 30 day sample')

    timestamps = get_timestamps_past_n_days(days_back)
    logger.debug('Num timestamps: ' + str(len(timestamps)))

    dynamo = SentryAWSClients.SentryDynamo()
    cameras_properties = dynamo.get_cameras_properties_for_sentry_id(sentry_id)

    camera_ids = []
    received_keys_list = []
    for camera in cameras_properties:
        camera_ids.append(camera['camera_id'])

    for camera_id in camera_ids:
        for timestamp in timestamps:
            cam_images = dynamo.get_images_for_camera_for_day(camera_id, timestamp)
            trimmed_cam_images = trim_list_greater_than_n(cam_images, trim_to)
            for image in trimmed_cam_images:
                if 'received_key' in image:
                    received_keys_list.append(image["received_key"])

    return received_keys_list


#  Trims list to n items +-1
def trim_list_greater_than_n(in_list, n):
    out_list = []
    num_items = len(in_list)
    num_to_skip = num_items - n

    if num_to_skip < 1:
        out_list = in_list
    else:
        skip_increment = round(num_items / n)

        for index, item in enumerate(in_list):
            if index % skip_increment == 0:
                out_list.append(item)

    return out_list


def get_received_keys_for_a_day(sentry_id, day):
    logger.debug('Day timestamp: ' + str(day))

    if type(day) is str:
        day = int(day)
    if day < time.time() + 3 - 31 * 86400:
        return []

    dynamo = SentryAWSClients.SentryDynamo()
    cameras_properties = dynamo.get_cameras_properties_for_sentry_id(sentry_id)

    camera_ids = []
    received_keys_list = []
    for camera in cameras_properties:
        camera_ids.append(camera['camera_id'])

    for camera_id in camera_ids:
        cam_images = dynamo.get_images_for_camera_for_day(camera_id, day)
        for image in cam_images:
            if 'received_key' in image:
                received_keys_list.append(image["received_key"])
    return received_keys_list


def lambda_handler(event, _):
    sentry_id = event["sentry_id"]
    timestamp = event["timestamp"]
    logger.debug('Event: ' + str(event))

    if timestamp.startswith('db_'):
        timestamp.strip('db_')
        r_list = get_m_keys_per_day_for_last_n_days(sentry_id, 30)
    else:
        r_list = get_received_keys_for_a_day(sentry_id, timestamp)

    url_dict = {}

    # Generate the URL to get 'key-name' from 'bucket-name'
    sentry_s3 = SentryAWSClients.SentryS3()
    for key in r_list:
        url = sentry_s3.get_timed_s3_url(PipelineConfig.IMAGES_BUCKET_NAME, key, 3600)
        url_dict.update({key: url})

    logger.debug('Number of image urls: ' + str(len(url_dict)))
    return url_dict
