import lambda_function
import time


def test_lambda_bad_id():
    event = {'sentry_id': 'bad_id', 'timestamp': '1234'}

    url_dict = lambda_function.lambda_handler(event, None)

    assert url_dict == {}


def test_lambda():
    #  'S-J3FKD5' is on dev
    ts = "1574713594"
    event = {'sentry_id': 'S-J3FKD5', 'timestamp': ts}

    url_list = lambda_function.lambda_handler(event, None)

    assert 'https://sentry-images-bucket.s3.amazonaws.com/S-J3FKD5_test_1574684044.jpg' \
           in url_list['S-J3FKD5_test_1574684044.jpg']


def test_lambda_thirty():
    ts = "db_1574713594"
    event = {'sentry_id': 'S-J3FKD5', 'timestamp': ts}

    url_list = lambda_function.lambda_handler(event, None)
    assert url_list is not None
    assert len(url_list) == 137


def test_lambda_in_glacier():
    #  'S-J3FKD5' is on dev
    ts = str(int(time.time() - 50 * 86400))
    event = {'sentry_id': 'S-J3FKD5', 'timestamp': ts}

    url_list = lambda_function.lambda_handler(event, None)

    assert url_list == {}


# def test_get_ten_keys_per_day_for_last_n_days():
#     keys = lambda_function.get_ten_keys_per_day_for_last_n_days('S-J3FKD5', 19)
#
#     assert len(keys) == 8

# <class 'dict'>: {'S-J3FKD5_test_1574684044.jpg': 'https://sentry-images-bucket.s3.amazonaws.
# com/S-J3FKD5_test_1574684044.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=
# AKIAUOTQKFBBUJE644UT%2F20191125%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20191125T205333Z&X
# -Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=
# a433692da675d7997adbb180f968368343cd5eb45192873bf57165c80e1e3171',
# 'S-J3FKD5_test_1574684838.jpg': 'https://sentry-images-bucket.s3.
# amazonaws.com/S-J3FKD5_test_1574684838.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-
# Amz-Credential=AKIAUOTQKFBBUJE644UT%2F20191125%2Fus-west-2%2Fs3%2Faws4_request&X-
# Amz-Date=20191125T205333Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-
# Amz-Signature=7516963bb44e7d1cf925f5d39d6f469b80d33baf27510a997191a3784098adb2'}
