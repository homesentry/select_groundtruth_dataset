import os
os.environ['pipeline'] = "Production"

import lambda_function
import time
import math


#  This test will break when the image goes to glacier
# This can be useful to verify s3v4 security. s3v4 is not needed on the dev S3 bucket but is on production
def test_lambda():
    #  'S-J3FKD5' is on dev, 'JW2ZKG-B' is on production
    ts = "1574797599"
    event = {'sentry_id': 'JW2ZKG-B', 'timestamp': ts}

    url_list = lambda_function.lambda_handler(event, None)

    assert 'https://sentry-received-images.s3.amazonaws.com/JW2ZKG-B_chip_test_cam_1574746861.jpg' \
           in url_list['JW2ZKG-B_chip_test_cam_1574746861.jpg']


def test_lambda_images_in_glacier():
    #  'S-J3FKD5' is on dev, 'JW2ZKG-B', 9LGRKA-B are on production
    ts = str(math.floor(time.time() - 50 * 86400))
    event = {'sentry_id': '9LGRKA-B', 'timestamp': ts}

    url_list = lambda_function.lambda_handler(event, None)

    assert len(url_list) == 0
    assert url_list == {}
