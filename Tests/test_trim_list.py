import lambda_function


def test_trim_list_greater_than_n_is_less_than():
    in_list = [1, 2, 3, 4, 5, 6]
    n = 10

    out_list = lambda_function.trim_list_greater_than_n(in_list, n)

    assert out_list == in_list


def test_trim_list_greater_than_n_is_equal():
    in_list = [1, 2, 3, 4, 5, 6]
    n = 6

    out_list = lambda_function.trim_list_greater_than_n(in_list, n)

    assert out_list == in_list


def test_trim_list_greater_than_n_is_greater():
    in_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    n = 6

    out_list = lambda_function.trim_list_greater_than_n(in_list, n)

    print(out_list)

    assert out_list != in_list
    assert out_list == [1, 3, 5, 7, 9]
    assert len(out_list) == 5


def test_trim_list_greater_than_n_is_much_greater():
    in_list = []
    for i in range(637):
        in_list.append(i)
    n = 6

    out_list = lambda_function.trim_list_greater_than_n(in_list, n)

    print(out_list)

    assert out_list != in_list
    assert len(out_list) == 7
    assert out_list == [0, 106, 212, 318, 424, 530, 636]


def test_get_timestamps():
    timestamps = lambda_function.get_timestamps_past_n_days(30)

    assert len(timestamps) == 30
    assert int(timestamps[10] - timestamps[9]) == 86400